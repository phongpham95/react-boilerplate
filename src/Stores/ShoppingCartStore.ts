import { action, computed, observable } from 'mobx';

import { IProduct } from '../Components/ListItem/ListItem';

export interface IProductCart extends IProduct {
  amount: number;
}

export class ShoppingCartStore {
  @observable
  cart: IProductCart[] = [];

  @computed
  get total() {
    const item = this.cart.reduce((all, item) => {
      all += item.amount;
      return all;
    }, 0);
    const price = this.cart.reduce((all, item) => {
      all += item.age * item.amount;
      return all;
    }, 0);
    return { item, price };
  }

  @action
  addItem = (product: IProductCart, amount: number = 1) => {
    const index = this.cart.findIndex(item => item._id === product._id);

    if (index > -1) {
      const total = (this.cart[index].amount += amount);

      if (total > 0) {
        this.cart = [
          ...this.cart.slice(0, index),
          {
            ...product,
            amount: total,
          },
          ...this.cart.slice(index + 1),
        ];
      } else {
        this.removeItem(product);
      }
    } else {
      this.cart = [
        ...(this.cart || []),
        {
          ...product,
          amount: 1,
        },
      ];
    }
  }

  @action
  removeItem = (product: IProductCart) => {
    this.cart = this.cart.filter(item => item._id !== product._id);
  }
}

export default new ShoppingCartStore();
