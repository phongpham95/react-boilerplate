import { createContext, useContext } from 'react';

import authStore from './AuthStore';
import commonStore from './CommonStore';
import layoutStore from './LayoutStore';
import shoppingCartStore from './ShoppingCartStore';
import userStore from './UserStore';

export const storesContext = createContext({
  authStore,
  commonStore,
  shoppingCartStore,
  layoutStore,
  userStore,
});

export const useStores = () => useContext(storesContext);
