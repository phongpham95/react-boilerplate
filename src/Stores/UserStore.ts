import gql from 'graphql-tag';
import { get } from 'lodash';
import { action, computed, observable } from 'mobx';

import client from '../Graphqls/client';
import authStore from './AuthStore';

const GET_USER = gql`
  query {
    me {
      data {
        _id
        name
        username
        password
        email
        profileId
        profile {
          _id
          name
          display
          description
          roles
        }
      }
    }
  }
`;

class UserStore {
  @observable
  currentUser: any = null;

  @observable
  loadingUser: boolean = false;

  @observable
  updatingUser: boolean = false;

  @computed
  get roles() {
    return this.currentUser && get(this.currentUser, 'profile.roles');
  }

  @action
  getUser = async () => {
    this.loadingUser = true;
    const res: any = await client.query({
      query: GET_USER,
    });

    const { data, error } = res.data.me;

    if (error) {
      authStore.logout();
    } else {
      this.currentUser = data;
      this.loadingUser = false;
    }
  };

  //   @action
  //   updateUser(newUser) {
  //     this.updatingUser = true;
  //     return agent.Auth.save(newUser)
  //       .then(action(({ user }) => { this.currentUser = user; }))
  //       .finally(action(() => { this.updatingUser = false; }))
  //   }

  @action forgetUser() {
    this.currentUser = undefined;
  }
}

export default new UserStore();
