import gql from 'graphql-tag';
import { action, observable } from 'mobx';

import { ErrorNoti, SuccessNoti } from '../Components/UI';
import client from '../Graphqls/client';
import commonStore from './CommonStore';
import userStore from './UserStore';

const LOGIN = gql`
  mutation Login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      authToken
      error {
        title
        message
      }
    }
  }
`;

export class AuthStore {
  @observable
  inProgress: boolean = false;

  @action
  login = async (username: string, password: string) => {
    this.inProgress = true;

    const res: any = await client.mutate({
      mutation: LOGIN,
      variables: { username, password },
    });

    const { authToken, error } = res.data.login;
    if (error) {
      ErrorNoti(error.message);
    } else {
      commonStore.setToken(authToken);
      await userStore.getUser();
      SuccessNoti('Đăng nhập');
      this.inProgress = false;
    }
  };

  @action
  logout = () => {
    commonStore.setToken(null);
    userStore.forgetUser();
    ErrorNoti('Hết phiên đăng nhập!');
    return window.location.reload();
  };
}

export default new AuthStore();
