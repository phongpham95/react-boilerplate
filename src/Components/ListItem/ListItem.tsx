import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { get } from 'lodash';
import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import Item from '../../Components/Item/Item';
import { ErrorNoti } from '../../Components/UI';

const StyledContainer = styled.div`
  padding-top: 20px;
  text-align: center;
`;

const StyledListItem = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  @media only screen and (max-width: 768px) {
    width: 100%;
  }
  @media screen and (min-width: 992px) {
    padding: 0 10%;
  }
`;

const StyledTitle = styled.div`
  font-size: 22px;
  margin-bottom: 10px;
`;

const StyledLink = styled(Link)`
  display: block;
  font-size: 12px;
  text-decoration: underline;
`;

export interface IProduct {
  _id: string;
  name: string;
  age: number;
  breed: string;
}

const FIND_MANY_CAT = gql`
  query FindManyCat($where: CatFilter, $take: Int, $order: CatSort) {
    findManyCat(where: $where, take: $take, order: $order) {
      _id
      name
      age
      breed
      creatorId
      creator {
        name
        username
      }
      customerId
      customer {
        name
        username
      }
      createdAt
      updatedAt
    }
  }
`;

interface IProps {
  title?: string;
  link?: string;
  where?: object;
  take?: number;
  isNewest?: boolean;
}

const ListItem: FC<IProps> = ({ where, take, isNewest = true, title, link }) => {
  const { loading, error, data } = useQuery(FIND_MANY_CAT, {
    variables: { where, take, order: { createdAt: isNewest ? 'DESC' : 'ASC' } },
    fetchPolicy: 'network-only',
  });

  if (error) {
    ErrorNoti(error.message);
  }

  const products: IProduct[] = get(data, 'findManyCat', []);

  return (
    <StyledContainer>
      <StyledTitle>
        {title}
        {link && <StyledLink to={link}>Xem thêm</StyledLink>}
      </StyledTitle>
      <StyledListItem>
        {!loading && products.map((item: IProduct) => <Item key={item._id} item={item} />)}
      </StyledListItem>
    </StyledContainer>
  );
};

export default ListItem;
