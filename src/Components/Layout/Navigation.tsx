import { Icon, Layout, Menu } from 'antd';
import { get } from 'lodash';
import { observer } from 'mobx-react';
import React, { FC, useState } from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import { routes } from '../../Screens/Routes';
import { useStores } from '../../Stores';

const { Sider } = Layout;
const { Item, SubMenu } = Menu;

const StyledSider = styled(Sider)`
  height: 100vh;
  overflow: auto;
  position: fixed;
  left: 0;
  top: 0;
  z-index: 1;
`;

const StyledMenu = styled(Menu)`
  line-height: 64;
`;

const Navigation: FC<RouteComponentProps> = observer(() => {
  const {
    authStore: { logout },
    userStore: { currentUser },
    layoutStore: { isSiderCollapse, toggleSider },
  } = useStores();

  const [isMobile, setMobile] = useState(window.innerWidth < 768 ? true : false);

  return (
    <StyledSider
      collapsible={true}
      collapsedWidth={isMobile ? 0 : 80}
      breakpoint='md'
      onBreakpoint={() => {
        setMobile(window.innerWidth < 768 ? true : false);
      }}
      onCollapse={collapse => {
        toggleSider(collapse);
      }}
      collapsed={isSiderCollapse}
    >
      <StyledMenu theme='dark' mode={isMobile ? 'vertical' : 'inline'}>
        {currentUser && (
          <SubMenu
            title={
              <span>
                <Icon type='user' />
                <span>{get(currentUser, 'name')}</span>
              </span>
            }
          >
            <Item key='1'>
              <Link to='/info'>
                <Icon type='user' />
                <span>Thông tin cá nhân</span>
              </Link>
            </Item>
            <Item key='2' onClick={() => logout()}>
              <Icon type='logout' />
              <span>Thoát</span>
            </Item>
          </SubMenu>
        )}

        {routes
          .filter(route => {
            if (!currentUser && route.isPrivate) {
              return false;
            }
            return true;
          })
          .map(route =>
            route.subRoutes ? (
              <SubMenu
                key={route.name}
                title={
                  <span>
                    <Icon type={route.icon} />
                    <span>{route.name}</span>
                  </span>
                }
              >
                {route.subRoutes.map(subRoute => (
                  <Item key={subRoute.name}>
                    <Link to={route.path + subRoute.path}>
                      <Icon type={subRoute.icon} />
                      <span>{subRoute.name}</span>
                    </Link>
                  </Item>
                ))}
              </SubMenu>
            ) : (
              <Item key={route.name}>
                <Link to={route.path}>
                  <Icon type={route.icon} />
                  <span>{route.name}</span>
                </Link>
              </Item>
            )
          )}
      </StyledMenu>
    </StyledSider>
  );
});

export default withRouter(Navigation);
