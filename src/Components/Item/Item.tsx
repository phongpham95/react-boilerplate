import { Button, Icon } from 'antd';
import { get } from 'lodash';
import { observer } from 'mobx-react';
import React, { FC } from 'react';
import styled from 'styled-components';

import { useStores } from '../../Stores';
import { IProductCart } from '../../Stores/ShoppingCartStore';
import { IProduct } from '../ListItem/ListItem';

const StyledImage = styled.img`
  width: 100%;
`;

const StyledContent = styled.div`
  padding: 10px;
`;

const StyledName = styled.div`
  color: black;
  font-size: 20px;
`;

const StyledPrice = styled.div`
  color: #5b5a5e;
  font-size: 18px;
`;

// const StyledButton = styled.div`
//   border: none;
//   outline: 0;
//   padding: 12px;
//   color: white;
//   background-color: #001529;
//   text-align: center;
//   cursor: pointer;
//   width: 100%;
//   font-size: 18px;

//   &:hover {
//     background-color: #5b5a5e;
//   }
// `;

const StyledButton = styled(Button)``;

const StyledBox = styled.div`
  padding: 10px;
  margin-bottom: 10px;
  text-align: center;

  &:hover {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  }

  @media only screen and (min-width: 1600px) {
    width: 20%;
  }

  @media only screen and (max-width: 1600px) {
    width: 25%;
  }

  @media only screen and (max-width: 1200px) {
    width: 33.33%;
  }

  @media only screen and (max-width: 576px) {
    width: 50%;
  }
`;

interface IProps {
  item: IProduct;
}

const Item: FC<IProps> = observer(({ item }) => {
  const {
    shoppingCartStore: { addItem },
  } = useStores();

  const product: IProductCart = { ...item, amount: 1 };

  return (
    <StyledBox>
      <StyledImage alt={get(item, 'name')} src='https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png' />
      <StyledContent>
        <StyledName>{get(item, 'name')}</StyledName>
        <StyledPrice>{`$ ${get(item, 'age')}`}</StyledPrice>
      </StyledContent>
      <StyledButton type='primary' onClick={() => addItem(product)} block={true} size='large'>
        <Icon type='shopping-cart' />
      </StyledButton>
    </StyledBox>
  );
});

export default Item;
