import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { HttpLink } from 'apollo-link-http';

import { ErrorNoti } from '../Components/UI';
import commonStore from '../Stores/CommonStore';

const httpLink = new HttpLink({ uri: 'http://localhost:3000/graphql' });

const authLink = setContext((_, { headers }) => {
  const { token } = commonStore;

  const reqHeaders = {
    ...headers,
  };
  if (token) {
    reqHeaders.authorization = token;
  }
  return { headers: reqHeaders };
});

const httpLinkWithAuthToken = authLink.concat(httpLink);

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors) {
        graphQLErrors.map(({ message, locations, path }) => {
          console.error(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);

          return ErrorNoti(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
        });
      }
      if (networkError) {
        ErrorNoti(`[Network error]:${networkError}`);
      }
    }),
    httpLinkWithAuthToken,
  ]),
  cache: new InMemoryCache({}),
});

(window as any).__APOLLO_CLIENT__ = client;

export default client;
