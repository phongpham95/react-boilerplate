import { Button, Col, Form, Icon, Input, Row } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { observer } from 'mobx-react';
import React, { FC, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router';
import styled from 'styled-components';

import { useStores } from '../../Stores';

const { Item } = Form;
const { Password } = Input;

const StyledH1 = styled.h1`
  text-align: center;
`;

const StyledIcon = styled(Icon)`
  color: rgba(0, 0, 0, 0.25);
`;

const Login: FC<FormComponentProps> = observer(({ form }) => {
  const {
    authStore: { login },
    userStore: { currentUser },
  } = useStores();

  const history = useHistory();
  const location = useLocation();

  const { from } = location.state || { from: { pathname: '/' } };

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    await form.validateFields(async (err, { username, password }) => {
      if (!err) {
        await login(username, password);
      }
    });
  };

  useEffect(() => {
    if (currentUser) {
      history.replace(from);
    }
  });

  const { getFieldDecorator } = form;

  return (
    <Row>
      <Col md={{ span: 6, offset: 8 }}>
        <StyledH1>Login</StyledH1>
        <Form onSubmit={e => handleSubmit(e)}>
          <Item>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Vui lòng nhập tên đăng nhập!' }],
            })(<Input prefix={<StyledIcon type="user" />} placeholder="Username" />)}
          </Item>
          <Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Vui lòng nhập mật khẩu!' }],
            })(<Password prefix={<StyledIcon type="lock" />} type="password" placeholder="Password" />)}
          </Item>
          <Item>
            <Button type="primary" block={true} htmlType="submit">
              Log in
            </Button>
          </Item>
        </Form>
      </Col>
    </Row>
  );
});

export default Form.create()(Login);
