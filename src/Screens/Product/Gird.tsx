import { ColDef, ColGroupDef } from 'ag-grid-community';
import { observer } from 'mobx-react';
import React, { FC } from 'react';
import ReactResizeDetector from 'react-resize-detector';

import AgGridTable from '../../Components/AgGridTable/AgGridTable';
import GridCellActionRenderer from './GridCellActionRenderer';
import productStore from './Store';

const columnDefs: Array<ColDef | ColGroupDef> = [
  {
    headerName: '',
    field: '_id',
    maxWidth: 88,
    minWidth: 88,
    pinned: 'left',
    sortable: false,
    cellRendererFramework: GridCellActionRenderer,
  },
  {
    headerName: 'Name',
    field: 'name',
  },
  {
    headerName: 'Age',
    field: 'age',
  },
  {
    headerName: 'Breed',
    field: 'breed',
  },
];

const ProductGird: FC = observer(() => {
  const { data, loading, quickFilterText } = productStore;

  return (
    <ReactResizeDetector handleHeight={true}>
      {(size: any) => (
        <AgGridTable
          columnDefs={columnDefs}
          loading={loading}
          rowData={data}
          quickFilterText={quickFilterText}
          height={size.height}
        />
      )}
    </ReactResizeDetector>
  );
});

export default ProductGird;
