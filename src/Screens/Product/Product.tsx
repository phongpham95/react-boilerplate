import React, { FC } from 'react';
import styled from 'styled-components';

import ProductForm from './Form';
import ProductGird from './Gird';
import ProductHeader from './Header';

const StyledContainer = styled.div`
  height: 100%;
  background: #fff;
  display: flex;
  flex-flow: column;
`;

const Product: FC = () => (
  <StyledContainer>
    <ProductHeader />
    <div style={{ flex: 1, padding: '0 12px 12px 12px' }}>
      <ProductGird />
    </div>
    <ProductForm />
  </StyledContainer>
);

export default Product;
