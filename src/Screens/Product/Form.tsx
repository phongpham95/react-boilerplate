import { Col, Form, Input, InputNumber, Modal, Row, Spin } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { get } from 'lodash';
import { observer } from 'mobx-react';
import React, { FC } from 'react';
import { useModalForm } from 'sunflower-antd';

import productStore from './Store';

const { Item } = Form;

const ProductForm: FC<FormComponentProps> = observer(({ form }) => {
  const { modalVisible, setModalVisible, createCat, updateCat, selectedItem } = productStore;
  const { getFieldDecorator, resetFields } = form;

  const { modalProps, formProps, formLoading } = useModalForm({
    form,
    autoResetForm: true,
    async submit(record) {
      if (get(selectedItem, '_id')) {
        await updateCat(get(selectedItem, '_id'), record);
      } else {
        await createCat(record);
      }
      return 'ok';
    },
  });

  const onCancel = () => {
    resetFields();
    setModalVisible();
  };

  return (
    <Modal
      {...modalProps}
      title='Basic Modal'
      okText='Tạo'
      cancelText='Đóng'
      visible={modalVisible}
      onCancel={() => onCancel()}
    >
      <Spin spinning={formLoading}>
        <Form layout='vertical' {...formProps}>
          <Row gutter={12}>
            <Col md={{ span: 18 }}>
              <Item label='Tên'>
                {getFieldDecorator('name', {
                  rules: [{ required: true, message: 'Vui lòng nhập tên!' }],
                  initialValue: get(selectedItem, 'name') || '',
                })(<Input placeholder='Nhập tên' />)}
              </Item>
            </Col>
            <Col md={{ span: 6 }}>
              <Item label='Tuổi'>
                {getFieldDecorator('age', {
                  rules: [],
                  initialValue: get(selectedItem, 'age') || 1,
                })(<InputNumber placeholder='Nhập tuổi' style={{ width: '100%' }} />)}
              </Item>
            </Col>
          </Row>
          <Item label='Giống'>
            {getFieldDecorator('breed', {
              rules: [],
              initialValue: get(selectedItem, 'breed') || '',
            })(<Input placeholder='Nhập giống' />)}
          </Item>
        </Form>
      </Spin>
    </Modal>
  );
});

export default Form.create()(ProductForm);
