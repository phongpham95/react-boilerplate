import { Button, Popconfirm } from 'antd';
import { observer } from 'mobx-react';
import React, { FC } from 'react';

import productStore from './Store';

const { Group } = Button;

const GridCellActionRenderer: FC<any> = observer(({ value, data }) => {
  const { removeCat, setModalVisible } = productStore;

  return (
    <Group>
      <Button type='primary' icon='edit' onClick={() => setModalVisible(data)} />
      <Popconfirm title='Xóa?' cancelText='Không' okText='Đồng ý' onConfirm={() => removeCat(value)}>
        <Button type='danger' icon='delete' />
      </Popconfirm>
    </Group>
  );
});

export default GridCellActionRenderer;
