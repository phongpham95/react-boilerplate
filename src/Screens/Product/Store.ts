import gql from 'graphql-tag';
import { get } from 'lodash';
import { action, observable } from 'mobx';

import { ErrorNoti, SuccessNoti } from '../../Components/UI';
import client from '../../Graphqls/client';

export const FIND_MANY_CAT = gql`
  query FindManyCat($where: CatFilter, $take: Int, $skip: Int, $order: CatSort) {
    findManyCat(where: $where, take: $take, skip: $skip, order: $order) {
      _id
      name
      age
      breed
      creatorId
      creator {
        name
        username
      }
      customerId
      customer {
        name
        username
      }
      createdAt
      updatedAt
    }
  }
`;

export const CREATE_CAT = gql`
  mutation CreateCat($record: CatInput!) {
    createCat(record: $record) {
      data {
        _id
        name
        age
        breed
        creatorId
        creator {
          name
          username
        }
        customerId
        customer {
          name
          username
        }
        createdAt
        updatedAt
      }
      error {
        title
        message
      }
    }
  }
`;

export const UPDATE_CAT = gql`
  mutation UpdateCatById($_id: ID!, $record: CatInput!) {
    updateCatById(_id: $_id, record: $record) {
      data {
        _id
        name
        age
        breed
        creatorId
        creator {
          name
          username
        }
        customerId
        customer {
          name
          username
        }
        createdAt
        updatedAt
      }
      error {
        title
        message
      }
    }
  }
`;

export const REMOVE_CAT_BY_ID = gql`
  mutation RemoveCatById($_id: ID!) {
    removeCatById(_id: $_id) {
      data {
        _id
        name
        age
        breed
        creatorId
        creator {
          name
          username
        }
        customerId
        customer {
          name
          username
        }
        createdAt
        updatedAt
      }
      error {
        title
        message
      }
    }
  }
`;

export class ProductStore {
  @observable
  loading: boolean = true;

  @observable
  data: object[] | [] = [];

  @observable
  selectedItem: object | null = null;

  @observable
  quickFilterText: string | undefined = undefined;

  @observable
  modalVisible: boolean = false;

  initialized: boolean = false;

  constructor() {
    this.init();
  }

  @action
  init = async (forceReinit: boolean = false) => {
    if (this.initialized && !forceReinit) {
      return;
    }
    await this.fetchData().finally(
      action(() => {
        this.loading = false;
        this.initialized = true;
      })
    );
  }

  @action
  setQuickFilterText = (text: string) => {
    this.quickFilterText = text;
  }

  @action
  setModalVisible = (item = null) => {
    this.selectedItem = item;
    this.modalVisible = !this.modalVisible;
  }

  @action
  fetchData = async (where?: object, take?: number, skip?: number, order?: object) => {
    const {
      data: { findManyCat },
    } = await client.query({
      query: FIND_MANY_CAT,
      variables: { where, take, skip, order },
      // fetchPolicy: 'network-only',
    });

    this.data = findManyCat || [];
  }

  @action
  createCat = async (record: any) => {
    const { data: resData } = await client.mutate({
      mutation: CREATE_CAT,
      variables: { record },
    });

    const {
      createCat: { error, data },
    } = resData;

    if (error) {
      ErrorNoti(error.message);
    } else {
      this.updateData(undefined, data);
      this.setModalVisible();
      SuccessNoti('Tạo sản phẩm');
    }
  }

  @action
  updateCat = async (_id: string, record: any) => {
    const { data: resData } = await client.mutate({
      mutation: UPDATE_CAT,
      variables: { _id, record },
    });

    const {
      updateCatById: { error, data },
    } = resData;
    if (error) {
      ErrorNoti(error.message);
    } else {
      this.updateData(_id, data);
      this.setModalVisible();
      SuccessNoti('Cập nhật sản phẩm');
    }
  }

  @action
  removeCat = async (_id: string) => {
    const { data: resData } = await client.mutate({
      mutation: REMOVE_CAT_BY_ID,
      variables: { _id },
    });

    const { error } = resData;
    if (error) {
      ErrorNoti(error.message);
    } else {
      this.updateData(_id);
      SuccessNoti('Xoá sản phẩm');
    }
  }

  @action
  updateData = (_id?: string, record?: any) => {
    if (!_id) {
      this.data = this.data.concat(record);
    } else {
      const index = this.data.findIndex(item => get(item, '_id') === _id);

      this.data = [...this.data.slice(0, index), record, ...this.data.slice(index + 1)].filter(Boolean);
    }
  }
}

export default new ProductStore();
