import { Button, Icon, Input, PageHeader } from 'antd';
import { observer } from 'mobx-react';
import React, { FC } from 'react';

import productStore from './Store';

const ProductHeader: FC = observer(() => {
  const { setQuickFilterText, setModalVisible } = productStore;

  return (
    <PageHeader
      title='Danh sách sản phẩm'
      extra={[
        <Input
          key='search'
          suffix={<Icon type='search' />}
          placeholder='Tìm kiếm...'
          allowClear={true}
          onChange={e => setQuickFilterText(e.target.value)}
          style={{ width: 200 }}
        />,
        <Button key='create' type='primary' icon='plus' onClick={() => setModalVisible()}>
          Tạo mới
        </Button>,
      ]}
    />
  );
});

export default ProductHeader;
