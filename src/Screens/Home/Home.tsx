import React from 'react';
import styled from 'styled-components';

import ListItem from '../../Components/ListItem/ListItem';
import SlideBanner from '../../Components/SlideBanner/SlideBanner';

const StyledContainer = styled.div`
  height: 100%;
  background: #fff;
`;

const Home = () => {
  const where = {};

  const width = window.innerWidth;

  let take = 6;
  if (1200 < width && width < 1600) {
    take = 8;
  } else if (1600 < width) {
    take = 10;
  }

  return (
    <StyledContainer>
      <SlideBanner />
      <ListItem title='Sản phẩm mới' where={where} take={take} isNewest={true} />
    </StyledContainer>
  );
};

export default Home;
