import { observer } from 'mobx-react';
import React, { ComponentType, FC } from 'react';
import { Redirect, Route, RouteComponentProps, RouteProps, Switch } from 'react-router-dom';

import { useStores } from '../Stores';
import Dashboard from './Dashboard/Dashboard';
import Home from './Home/Home';
import Login from './Login/Login';
import Product from './Product/Product';
import TodoList from './Todo/TodoList';

export interface IRoute {
  path: string;
  exact?: boolean;
  isPrivate?: boolean;
  name: string;
  icon?: string;
  component: ComponentType<RouteComponentProps<any>> | ComponentType<any>;
  subRoutes?: IRoute[];
}

export const routes: IRoute[] = [
  {
    path: '/',
    exact: true,
    isPrivate: true,
    name: 'Home',
    icon: 'home',
    component: () => <Home />,
  },
  {
    path: '/todos',
    isPrivate: false,
    name: 'Todos',
    icon: 'check',
    component: () => <TodoList />,
  },
  {
    path: '/admin',
    isPrivate: true,
    name: 'Dashboard',
    icon: 'dashboard',
    component: () => <Dashboard />,
  },
  {
    path: '/product',
    isPrivate: true,
    name: 'Product',
    icon: 'skin',
    component: () => <Product />,
  },
];

const PrivateRoute: FC<RouteProps> = observer(props => {
  const {
    userStore: { currentUser },
  } = useStores();

  if (currentUser) {
    return <Route {...props} />;
  }

  return (
    <Redirect
      to={{
        pathname: '/login',
        state: { from: props.path },
      }}
    />
  );
});

const Routes = () => (
  <Switch>
    <Route path='/login' component={Login} />
    {routes.map(route =>
      route.isPrivate ? <PrivateRoute key={route.name} {...route} /> : <Route key={route.name} {...route} />
    )}
  </Switch>
);

export default Routes;
