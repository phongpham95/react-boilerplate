import { observer } from 'mobx-react';
import React from 'react';

import { TTodo } from './Store';

interface IProps {
  todo: TTodo;
}

export const Todo: React.FC<IProps> = observer(({ todo }) => {
  return <h1>{todo.text}</h1>;
});
