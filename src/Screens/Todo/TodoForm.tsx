import { Button, Input } from 'antd';
import { observer } from 'mobx-react';
import React, { Fragment, useState } from 'react';

import todoStore from './Store';

export const TodoForm: React.FC = observer(() => {
  const { addTodo } = todoStore;

  const [todo, setTodo] = useState('');

  const add = () => {
    addTodo(todo);
    setTodo('');
  };

  return (
    <Fragment>
      <Input onChange={e => setTodo(e.target.value)} value={todo}></Input>
      <Button type='primary' onClick={() => add()} disabled={!!!todo}>
        Add Todo
      </Button>
    </Fragment>
  );
});
