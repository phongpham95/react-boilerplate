import { action, observable } from 'mobx';

export interface TTodo {
  id: number;
  text: string;
  completed: boolean;
  userId: number;
}

const inititalTodo: TTodo[] = [
  { id: 0, text: 'Do Nest', completed: true, userId: 1 },
  { id: 1, text: 'Do React', completed: true, userId: 1 },
];

let id = 1;

export class TodoStore {
  @observable
  todos = [...inititalTodo] as TTodo[];

  @action
  addTodo = (text: string) => {
    const todo: TTodo = {
      id: id += 1,
      text,
      completed: false,
      userId: 1,
    };

    this.todos.push(todo);
  }
}

export default new TodoStore();
