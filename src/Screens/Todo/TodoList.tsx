import { observer } from 'mobx-react';
import React, { Fragment } from 'react';

import todoStore from './Store';
import { Todo } from './Todo';
import { TodoForm } from './TodoForm';

const TodoList: React.FC = observer(() => {
  const { todos } = todoStore;

  return (
    <Fragment>
      {todos.map(todo => (
        <Todo key={todo.id} todo={todo}></Todo>
      ))}
      <TodoForm />
    </Fragment>
  );
});

export default TodoList;
