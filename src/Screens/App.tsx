import { Badge, Button, Icon, Layout, Spin } from 'antd';
import { observer } from 'mobx-react';
import React, { FC, useEffect } from 'react';
import styled from 'styled-components';

import Navigation from '../Components/Layout/Navigation';
import ShoppingCart from '../Components/ShoppingCart/ShoppingCart';
import { useStores } from '../Stores';
import Routes from './Routes';

const { Content, Header } = Layout;

const StyledIcon = styled(Icon)`
  font-size: 20px;
  padding: 0 6px;
`;

const StyledHeader = styled(Header)`
  background: #fff;
  padding: 0;
`;

const App: FC = observer(() => {
  const {
    commonStore: { appLoaded, setAppLoaded },
    userStore: { getUser },
    layoutStore: { isSiderCollapse, toggleSider, toggleShoppingCart },
    shoppingCartStore: { total },
  } = useStores();

  const token = localStorage.getItem('token');

  useEffect(() => {
    if (!appLoaded && token) {
      getUser().finally(() => setAppLoaded());
    } else {
      setAppLoaded();
    }
  });

  const isMobile = window.innerWidth < 768;

  const header = { marginLeft: 0 };
  const content = { marginLeft: 0 };
  const icon = { marginRight: 0 };

  if (isMobile && !isSiderCollapse) {
    header.marginLeft = 200;
    icon.marginRight = 200;
  } else if (!isMobile && isSiderCollapse) {
    content.marginLeft = 80;
    icon.marginRight = 80;
  } else if (!isMobile && !isSiderCollapse) {
    content.marginLeft = 200;
    icon.marginRight = 200;
  }

  return (
    <Spin spinning={!appLoaded}>
      <Layout>
        <Navigation />
        <ShoppingCart />
        <Layout style={{ ...content }}>
          <StyledHeader style={{ ...header }}>
            <Button
              type='link'
              style={{ display: isMobile ? 'inline' : 'none', height: '100%' }}
              onClick={() => toggleSider()}
            >
              <StyledIcon type={isSiderCollapse ? 'menu-unfold' : 'menu-fold'} />
            </Button>
            <Button type='link' style={{ float: 'right', height: '100%' }} onClick={() => toggleShoppingCart()}>
              <Badge count={total.item} showZero={true}>
                <StyledIcon type='shopping-cart' />
              </Badge>
            </Button>
          </StyledHeader>
          <Layout style={{ minHeight: 'calc(100vh - 64px)' }}>
            <Content style={{ padding: 12, height: '100%' }}>
              <Routes />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </Spin>
  );
});

export default App;
